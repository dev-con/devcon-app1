FROM node:alpine
WORKDIR /app
COPY app/package.json .
RUN npm i
ARG VERSION=1.0
ENV VERSION=$VERSION
COPY app/ .
CMD npm start
